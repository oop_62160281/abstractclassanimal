/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author ASUS
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.run();
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));

        Cat c1 = new Cat("Dara");
        c1.run();
        c1.eat();
        c1.walk();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));

        Dog d1 = new Dog("Da");
        d1.run();
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));

        Crocodile cd1 = new Crocodile("Co");
        cd1.crawl();
        cd1.eat();
        cd1.walk();
        cd1.speak();
        cd1.sleep();
        System.out.println("cd1 is animal ? " + (cd1 instanceof Animal));
        System.out.println("cd1 is land animal ? " + (cd1 instanceof Reptile));

        Snake s1 = new Snake("No");
        s1.crawl();
        s1.eat();
        s1.walk();
        s1.speak();
        s1.sleep();
        System.out.println("s1 is animal ? " + (cd1 instanceof Animal));
        System.out.println("s1 is land animal ? " + (cd1 instanceof Reptile));

        Fish f1 = new Fish("Fo");
        f1.swim();
        f1.eat();
        f1.walk();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is AquaticAnimal ? "
                + (f1 instanceof AquaticAnimal));

        Crab cr1 = new Crab("Ro");
        cr1.swim();
        cr1.eat();
        cr1.walk();
        cr1.speak();
        cr1.sleep();
        System.out.println("cr1 is animal ? " + (cr1 instanceof Animal));
        System.out.println("cr1 is AquaticAnimal ? "
                + (cr1 instanceof AquaticAnimal));

        Bat b1 = new Bat("Bo");
        b1.fly();
        b1.eat();
        b1.walk();
        b1.speak();
        b1.sleep();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is Poultry ? " + (b1 instanceof Poultry));

        Bird bi1 = new Bird("Bio");
        bi1.fly();
        bi1.eat();
        bi1.walk();
        bi1.speak();
        bi1.sleep();
        System.out.println("bi1 is animal ? " + (bi1 instanceof Animal));
        System.out.println("bi1 is Poultry ? " + (bi1 instanceof Poultry));

        Animal a1 = h1;
        System.out.println("a1 is lanb animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animel ? " + (a1 instanceof Reptile));

        Animal a2 = c1;
        System.out.println("a2 is lanb animal ? " + (a2 instanceof LandAnimal));
        System.out.println("a2 is reptile animel ? " + (a2 instanceof Reptile));

        Animal a3 = d1;
        System.out.println("a3 is lanb animal ? " + (a3 instanceof LandAnimal));
        System.out.println("a3 is reptile animel ? " + (a3 instanceof Reptile));

        Animal a4 = cd1;
        System.out.println("a4 is Reptile ? " + (a4 instanceof Reptile));
        System.out.println("a4 is reptile animel ? " + (a4 instanceof LandAnimal));

        Animal a5 = s1;
        System.out.println("a5 is Reptile ? " + (a5 instanceof Reptile));
        System.out.println("a5 is reptile animel ? " + (a5 instanceof LandAnimal));

        Animal a6 = f1;
        System.out.println("a6 is Reptile ? " + (a6 instanceof AquaticAnimal));
        System.out.println("a6 is reptile animel ? " + (a6 instanceof LandAnimal));

        Animal a7 = cr1;
        System.out.println("a7 is Reptile ? " + (a7 instanceof AquaticAnimal));
        System.out.println("a7 is reptile animel ? " + (a7 instanceof LandAnimal));

        Animal a8 = b1;
        System.out.println("a8 is Poultry ? " + (a8 instanceof Poultry));
        System.out.println("a8 is reptile animel ? " + (a8 instanceof LandAnimal));
        
        Animal a9 = bi1;
        System.out.println("a9 is Poultry ? " + (a9 instanceof Poultry));
        System.out.println("a9 is reptile animel ? " + (a9 instanceof LandAnimal));

    }
}
